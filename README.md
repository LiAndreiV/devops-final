# DevOps Final

### Classifying Email as Spam or Non-Spam
The task is taken from the repository https://archive.ics.uci.edu/ml/datasets/spambase

The problem has two solutions - proper solution (https://gitlab.com/LiAndreiV/devops-final-main) and fallback solution (https://gitlab.com/LiAndreiV/devops-final-fallback)

The proper solution solves the problem using decision trees.
The fallback solution solves the problem using naive bayess.

To test the application, you need to pass 56 comma-separeted features as input. An example of such a file is in test/example_data.txt
The correctness of the program can be checked using the command:
curl -L -F "file=@/vagrant/test/example_data.txt" spam-predictor.ml

Application crash simulation:
kubectl scale deployment main-deployment

Check if fallback solution is in use:
curl -L -F "file=@/vagrant/test/example_data.txt" spam-predictor.ml
